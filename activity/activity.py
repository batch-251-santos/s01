# Activity

name = "Paul"
age = 68
occupation = "developer"
movie = "The Prestige"
rating = 92

num1, num2, num3 = 3, 12, 18

print(f"I am {name}, and I am {age} years old. I work as a {occupation} and my rating for the movie '{movie}' is {rating}%.")

print(f"The product of {num1} and {num2} is {num1 * num2}.")
print(f"{num1} is less than {num3}? {num1 < num3}.")
print(f"{num3} added to {num2} is equal to {num2 + num3}.")