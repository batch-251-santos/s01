print("Hello World!")

# [SECTION] Declaring Variables
age = 29
middle_initial = "D"


# Python allows assigning of values to multiple variables within a single line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

# [Section] Data Types
# 1. Strings - for alphanumeric characters and symbols
full_name = "John Dode"
secret_code = "Pa$$word"

# 2. Numbers - for integers, decimals and complex numbers
number_of_days = 365
pi_approximation = 3.1416
comlex_num = 1 + 5j

# 3. Boolean - true or false, capitalized
isLearning = True
isDifficult = False


# [SECTION] Using variables
print("My name is " + full_name)
print("My age is " + str(age)) # Typecasting, converting integer to string since we cannot concatenate str to str

print(int(3.5)) # prints 3
print(float(5))	# prints 5.0

# F-Strings
# Another way to concatenate and it ignores the strict typing needed with regular concatenation using the '+' operator
print(f"Hi! My name is {full_name} and my age is {age}.")


# [SECTION] Operations
# Arithmetic Operators - for mathematical operations
print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4)
print(2 ** 6)

# Assignment Operators
num1 = 3
num1 += 4
print(num1)

# Comparison Operators
print(1 == 1)

# Logical Operator
print(True and False)
print(not False)
print(False or True)